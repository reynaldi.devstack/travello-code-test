//
//  TravelloCodeTestTests.swift
//  TravelloCodeTestTests
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import XCTest
import RxSwift


@testable import TravelloCodeTest

let fakeUsers: [User] =
    [User(id: "1", userId: 1, title: "One test", body: "One test body", imageUrl: "https://picsum.photos/200", thumbnailUrl: "https://picsum.photos/100"),
    User(id: "2", userId: 2, title: "Two test", body: "Two test body", imageUrl: "https://picsum.photos/200", thumbnailUrl: "https://picsum.photos/100"),
    User(id: "3", userId: 3, title: "Three test", body: "Three test body", imageUrl: "https://picsum.photos/200", thumbnailUrl: "https://picsum.photos/100")]
let fakeApiService: ApiService = FakeApiService(users: fakeUsers)

class TravelloCodeTestTests: XCTestCase {

    let userViewModel = UserViewModel(apiService: fakeApiService)
    
    func testUserInfo () {
        userViewModel.getUsers()
        let users = userViewModel.users.value
        XCTAssertEqual(users[0].title, "One test")
        XCTAssertEqual(users[1].title, "Two test")
        XCTAssertEqual(users[2].title, "Three test")
    }

}
