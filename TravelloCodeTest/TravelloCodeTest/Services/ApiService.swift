//
//  ApiService.swift
//  TravelloCodeTest
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import Foundation

class ApiService {    
    func fetchUser(completion: @escaping ([User], Bool) -> ()){
        let url = URL(string: "https://5f30f899373bc7001635f179.mockapi.io/users")!
        
        URLSession.shared.dataTask(with: url){(data, response, error) in
            do{
                let users = try JSONDecoder().decode([User].self, from: data!)
                DispatchQueue.main.async {
                    completion(users, false)
                }
            }catch let err{
                completion([], true)
                print(err)
            }
        }.resume()
    }
}
