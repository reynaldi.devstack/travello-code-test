//
//  UserViewModel.swift
//  TravelloCodeTest
//
//  Created by DevStack MacBook on 10/08/20.
//  Copyright © 2020 DevStack. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class UserViewModel {
    
    // Create observer variable to retrieve data from user model
    var users = BehaviorRelay<[User]>(value: [])
    private let apiService: ApiService
    
    init(apiService: ApiService) {
        self.apiService = apiService
    }
    
    // fetch data from api
    func getUsers(){
        apiService.fetchUser() {
            users, error in
            self.users.accept(users)            
        }
    }
}
